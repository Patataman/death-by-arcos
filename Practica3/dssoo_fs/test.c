#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/filesystem.h"

#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_FONT_BOLD   "\x1b[1m"

int main() {
	int ret, /*i,*/ fd;
	/*char buffer1[BLOCK_SIZE];
	char buffer2[BLOCK_SIZE];
	char buffer3[BLOCK_SIZE];*/

fprintf(stdout, "%s", "TEST mkFS\n");
	/*Crear FS (5)
	-Casos limites de maxFiles y tamaño dispositivo.
	-Prueba buena.*/

//PRU-MF-01 (Crear sistema de ningún fichero y 400KB)
	ret = mkFS(0, 400000);
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-01 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-MF-02 (Crear sistema de fichero de más de los ficheros permitidos)
	ret = mkFS(51, 400000);
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-02 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-MF-03 (Crear sistema de fichero con menos del tamaño mínimo)
	ret = mkFS(30, 327679);
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-03 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-MF-04 (Crear sistema de ficheros con más del tamaño máximo)
	ret = mkFS(30, 512001);
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-04 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-04 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-MF-05 (Crear sistema de ficheros cone el nº máximo de ficheros)
	ret = mkFS(50, 400000);
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-05 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MF-05 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);




fprintf(stdout, "%s", "\nTEST mountFS and unmountFS\n");

//PRU-MNT-01 (Montar sistema)
	ret = mountFS();
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MNT-01 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MNT-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-UMNT-01 (Desmontar sistema)
	ret = umountFS();
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-UMNT-01 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}

	fprintf(stdout, "%s%s%s%s", "TEST PRU-UMNT-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-MNT-02 (Montar sistema después de haber sido desmontado)
	ret = mountFS();
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-MNT-02 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-MNT-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);





fprintf(stdout, "%s", "\nTEST creatFS\n");
//PRU-CF-01 (Nombre más largo de lo permitido)
	ret = creatFS("01234567890123456789012345678901234567890123456789012345678901234");
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-01 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-CF-02 (nombre invalido .) 
	ret = creatFS(".");
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-02 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-CF-03 (nombre invalido ..)
	ret = creatFS("..");
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-03 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-CF-04 (nombre invalido $)
	ret = creatFS("$");
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-04 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-04 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);		

//PRU-CF-05 (Crear fichero correctamente)
	ret = creatFS("test.txt");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-05 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-05 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-CF-06 (Crear dos ficheros con mismo nombre)
	ret = creatFS("test.txt");
	if(ret != 1) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-06 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-06 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);


fprintf(stdout, "%s", "\nTEST openFS\n");
//PRU-OPEN-01 (abrir 1 fichero)

	ret = creatFS("openTest.txt");

	fd = openFS("openTest.txt");
	if(fd < 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-01", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at openFS\n");		
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-01 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at listFS\n");

//PRU-OPEN-02 (Abrir varias veces el mismo fichero)
	fd = openFS("openTest.txt");
	if(fd < 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-02", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at openFS\n");		
	}

	int fd2 = openFS("openTest.txt");
	if(fd2 < 0 && fd != fd2) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-02", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at openFS\n");		
	}

	int fd3 = openFS("openTest.txt");
	if(fd3 < 0 && fd != fd3 && fd != fd2) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-02", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at openFS\n");		
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-02 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at listFS\n");


//PRU-OPEN-03 (Abrir fichero que no existe)
	int fd4 = openFS("openTestNoExiste.txt");
	if(fd4 != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-03", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at openFS\n");		
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-OPEN-03 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at listFS\n");


fprintf(stdout, "%s", "\nTEST closeFS\n");
//PRU-CLOSE-01 (Cerrar fichero existente)
	ret = closeFS(fd);
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-CLOSE-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at closeFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CLOSE-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-CLOSE-02 (cerrar fichero 2 veces seguidas)
	ret = closeFS(fd);
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-CLOSE-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at closeFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CLOSE-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-CLOSE-03 (cerrar fichero previamente no abierto)
	ret = closeFS(49);
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-CLOSE-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at closeFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CLOSE-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);


fprintf(stdout, "%s", "\nTEST readFS/closeFS/lseek\n");
//PRU-READ-01 (Leer un fichero vacío)
	char prueba[150];//buffer de lectura
	fd = openFS("openTest.txt");
	ret = readFS(fd, prueba, 150); //fichero vacío
	if(ret != 0){ //fichero vacío 
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-READ-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-READ-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-WRITE-01 (Escribir en un fichero)
	//Escribir 52 bytes
	ret = writeFS(fd, "Pues resulta que estamos escribiendo en el archivo.", 52);
	if(ret != 52){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-WRITE-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-WRITE-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-LSEEK-01 (Volver al comienzo de un fichero después de escribir)
	ret = lseekFS(fd, 0, FS_SEEK_BEGIN);
	if(ret < 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LSEEK-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-LSEEK-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-READ-02 (Leer un fichero con datos)
	ret = readFS(fd, prueba, 60); //fichero con 52 bytes
	if(ret != 52){ //fichero vacío 
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-READ-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-READ-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-WRITE-02 (Escribir al final del fichero)
	ret = writeFS(fd, "PUES RESULTA QUE ESTAMOS ESCRIBIENDO EN EL ARCHIVO.", 52);
	if(ret != 52){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-WRITE-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-WRITE-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-READ-03  (Leer fuera de rango)
	ret = readFS(fd, prueba, 60);
	if(ret != 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-READ-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-READ-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-READ-04 (Leer con descriptor negativo)
	ret = readFS(-3, prueba, 52); //fichero con 52 bytes
	if(ret != -1){ //fichero vacío 
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-READ-04 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-READ-04 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-READ-05 (Leer con descriptor no creado)
	ret = readFS(45, prueba, 52); //fichero con 52 bytes
	if(ret != -1){ //fichero vacío 
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-READ-05 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-READ-05 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-WRITE-03 (Escribir en un fichero no creado)
	ret = writeFS(43, "Texto para escribir en un fichero no creado", 43);
	if(ret != -1){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-WRITE-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-WRITE-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-WRITE-04 (Escribir con un descriptor negativo)
	ret = writeFS(-1, "Texto para escribir en un descriptor negativo", 45);
	if(ret != -1){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-WRITE-04 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-WRITE-04 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-WRITE-05 (Escribir mas bytes que cadena)
	ret = writeFS(fd, "YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO", 1324);
	ret = writeFS(fd, "YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO", 1324);
	ret = writeFS(fd, "YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO", 1324);
	ret = writeFS(fd, "YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO", 1324);
	ret = writeFS(fd, "YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO YokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKOYokSE NO SOI 100TIFIKO", 1324);
	if(ret != 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-WRITE-05 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-WRITE-05 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

fprintf(stdout, "%s", "\nTEST lseekFS\n");
//AQUI VAN LAS PRUEBAS DE LSEEK

//PRU-LSEEK-02 (Ir a un punto intermedio del fichero)
	ret = lseekFS(fd, 0, FS_SEEK_BEGIN);
	ret = lseekFS(fd, 40, FS_SEEK_SET);
	if(ret < 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LSEEK-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-LSEEK-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-LSEEK-03 (Ir fuera del fichero con rango negativo)
	ret = lseekFS(fd, 0, FS_SEEK_BEGIN);
	ret = lseekFS(fd, -23, FS_SEEK_SET);
	if(ret != -1){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LSEEK-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-LSEEK-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-LSEEK-04 (Ir fuera del fichero con rango positivo)
	ret = lseekFS(fd, 0, FS_SEEK_END);
	ret = lseekFS(fd, 40, FS_SEEK_SET);

	if(ret != -1){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LSEEK-04 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-LSEEK-04 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-LSEEK-05 (Ir al final del fichero)
	ret = lseekFS(fd, 0, FS_SEEK_END);
	if(ret != 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LSEEK-05 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-LSEEK-05 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-LSEEK-06 (lseek de un fichero no abierto)
	ret = lseekFS(45, 0, FS_SEEK_BEGIN);
	if(ret != -1){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LSEEK-06 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "\n");	
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-LSEEK-06 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);


	ret = closeFS(fd);
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-CLOSE-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at closeFS\n");
		return -1;
	}


fprintf(stdout, "%s", "\nTEST tagFS\n");

//PRU-TAG-01 //poner 1 etiqueta
	ret = tagFS("test.txt", "pepe");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-TAG-01 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);
	
//PRU-TAG-02 //poner 2 etiqueta
	ret = tagFS("test.txt", "pepe2");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-TAG-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-TAG-03 //poner 3 etiqueta
	ret = tagFS("test.txt", "pepe3");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-TAG-03 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);

//PRU-TAG-04 //poner 4 etiquetas (debe dar error)
	ret = tagFS("test.txt", "pepe4");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-04 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-04 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at tagFS\n");

//PRU-TAG-05 //Poner una etiqueta repetida
	ret = tagFS("test.txt", "pepe2");
	if(ret != 1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-05 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-05 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at tagFS\n");

//PRU-TAG-06 //Poner 30 etiquetas
	ret = creatFS("test2.txt");
	ret = creatFS("test3.txt");
	ret = creatFS("test4.txt");
	ret = creatFS("test5.txt");
	ret = creatFS("test6.txt");
	ret = creatFS("test7.txt");
	ret = creatFS("test8.txt");
	ret = creatFS("test9.txt");
	ret = creatFS("test10.txt");

	ret = tagFS("test2.txt", "etq1");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq1'  ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test2.txt", "etq2");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq2'  ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test2.txt", "etq3");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq3' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test3.txt", "etq4");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq4' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test3.txt", "etq5");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq5' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test3.txt", "etq6");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq6' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test4.txt", "etq7");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq7' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test4.txt", "etq8");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq8' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test4.txt", "etq9");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq9' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test5.txt", "etq10");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq10' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test5.txt", "etq11");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq11' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test5.txt", "etq12");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq12' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test6.txt", "etq13");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq13' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test6.txt", "etq14");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq14' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test6.txt", "etq15");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq15' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test7.txt", "etq16");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq16' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test7.txt", "etq17");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq17' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test7.txt", "etq18");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq18' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test8.txt", "etq19");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq19' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test8.txt", "etq20");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq20' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test8.txt", "etq21");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq21' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test9.txt", "etq22");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq22' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test9.txt", "etq23");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq23' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test9.txt", "etq24");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq24' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test10.txt", "etq25");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq25' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test10.txt", "etq26");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq26' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	ret = tagFS("test10.txt", "etq27");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 'etq27' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-06 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at tagFS\n");

//PRU-TAG-09 (etiqueta 31)
	ret = creatFS("test11.txt");
	if (ret != 0){
		return -1;
	}
	ret = tagFS("test11.txt", "etq31");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-09 'etq31' ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-09 'etq31' ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at tagFS\n");

//PRU-UNTAG-01 (quitar etiquetas que existen)
	ret = untagFS("test10.txt", "etq25");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-01 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

//PRU-UNTAG-02 (quitar todas las etiquetas de un fichero)
	ret = untagFS("test10.txt", "etq26");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-02 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

	ret = untagFS("test10.txt", "etq27");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-02 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

//PRU-UNTAG-03 (quitar etiqueta que no hay en un fichero)
	ret = untagFS("test10.txt", "wala ba dum da");
	if(ret != 1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-03 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

//PRU-UNTAG-04 (quitar etiqueta de un fichero abierto)
	fd = openFS("test10.txt");
	ret = untagFS("test10.txt", "wala ba dum da");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-04 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-04 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");
	closeFS(fd);

//PRU-TAG-07 (etiquetar un fichero abierto)
	fd = openFS("test10.txt");
	ret = tagFS("test10.txt", "wala ba dum da");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-07 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-07 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at tagFS\n");
	closeFS(fd);

//PRU-TAG-08 (etiquetar un fichero inexistente)
	ret = tagFS("ficheroQueNoExiste", "wala ba dum da");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-08 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at tagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-08 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at tagFS\n");

//PRU-TAG-10 (etiqueta de longitud 33)
ret = tagFS("test10.txt", "estaFraseEsSuperLargaPorqueDebeSe");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-10 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-TAG-10 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

//PRU-UNTAG-05 (desetiquetar un fichero inexistente)
	ret = untagFS("ficheroQueNoExiste", "pepe2");
	if(ret != -1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-05 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-05 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

//PRU-UNTAG-06 (desetiquetar 2 veces de un fichero)
	ret = untagFS("test9.txt", "etq23");
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-06 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	ret = untagFS("test9.txt", "etq23");
	if(ret != 1) {
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-06 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at untagFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-UNTAG-06 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at untagFS\n");

//PRU-LIST-01 (listar ficheros etiquetados con X)
	char **files = (char**)malloc(sizeof(char*)*3);
	int ii = 0;
	for(ii = 0; ii < 3; ii++){
		files[ii] = (char*) malloc(sizeof(char)*64);
	}
	
	ret = listFS("pepe", files);
	if(ret != 1 || strcmp(files[0], "test.txt") != 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LIST-01 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at listFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LIST-01 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at listFS\n");

//PRU-LIST-02 (listar ficheros etiquetados con X cuando no hay ninguno)
	ret = listFS("benancio", files);
	if(ret != 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LIST-02 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at listFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LIST-02 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at listFS\n");

//PRU-LIST-03 lisar etiqueta eliminada previamente
	ret = listFS("etq26", files);
	if(ret != 0){
		fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LIST-03 ", ANSI_COLOR_RED, "FAILED ", ANSI_COLOR_RESET, "at listFS\n");
		return -1;
	}
	fprintf(stdout, "%s%s%s%s%s", "TEST PRU-LIST-03 ", ANSI_COLOR_GREEN, "SUCCESS ", ANSI_COLOR_RESET, "at listFS\n");
	

	//A lo largo del test se han creado 12 ficheros, creamos 38 más
	ret = creatFS("test11.txt");
	ret = creatFS("test12.txt");
	ret = creatFS("test13.txt");
	ret = creatFS("test14.txt");
	ret = creatFS("test15.txt");
	ret = creatFS("test16.txt");
	ret = creatFS("test17.txt");
	ret = creatFS("test18.txt");
	ret = creatFS("test19.txt");
	ret = creatFS("test20.txt");

	ret = creatFS("test21.txt");
	ret = creatFS("test22.txt");
	ret = creatFS("test23.txt");
	ret = creatFS("test24.txt");
	ret = creatFS("test25.txt");
	ret = creatFS("test26.txt");
	ret = creatFS("test27.txt");
	ret = creatFS("test28.txt");
	ret = creatFS("test29.txt");
	ret = creatFS("test30.txt");

	ret = creatFS("test31.txt");
	ret = creatFS("test32.txt");
	ret = creatFS("test33.txt");
	ret = creatFS("test34.txt");
	ret = creatFS("test35.txt");
	ret = creatFS("test36.txt");
	ret = creatFS("test37.txt");
	ret = creatFS("test38.txt");
	ret = creatFS("test39.txt");
	ret = creatFS("test40.txt");

	ret = creatFS("test41.txt");
	ret = creatFS("test42.txt");
	ret = creatFS("test43.txt");
	ret = creatFS("test44.txt");
	ret = creatFS("test45.txt");
	ret = creatFS("test46.txt");
	ret = creatFS("test47.txt");
	ret = creatFS("test48.txt");


//PRU-CF-07 (crear fichero cuando ya existen el maximo permitido)
	ret = creatFS("test49.txt");
	if(ret == 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-07 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}
	fprintf(stdout, "%s%s%s%s", "TEST PRU-CF-07 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);
	
//PRU-UMNT-02 (Desmontar sistema despues de realizar operaciones)
	ret = umountFS();
	if(ret != 0) {
		fprintf(stdout, "%s%s%s%s", "TEST PRU-UMNT-02 ", ANSI_COLOR_RED, "FAILED\n", ANSI_COLOR_RESET);
		return -1;
	}

	fprintf(stdout, "%s%s%s%s", "TEST PRU-UMNT-02 ", ANSI_COLOR_GREEN, "SUCCESS\n", ANSI_COLOR_RESET);
	

	return 0;
}
