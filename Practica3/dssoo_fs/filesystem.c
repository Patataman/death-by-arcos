/* This file contains the definition of the functions that must be implemented
 * to allow the user access to the file system and the files.
 */

#include "include/filesystem.h"
#include <stdint.h>		/*uintXX_t*/
#include <stdlib.h>		/*malloc*/
#include <strings.h> 	/*bzero*/
#include <string.h> 	/*memcpy*/

/***************************/
/* File system management. */
/***************************/

/*
 * Formats a device.
 * Returns 0 if the operation was correct or -1 in case of error.
 */
//CONSTANTES
#define HASHTAG_SIZE 32			/*Tamaño máximo de una etiqueta*/
#define FILENAME_SIZE 64		/*Tamaño máximo del nombre de un archivo*/
#define HASHTAG_AMOUNT 30		/*Cantidad máxima de etiquetas en el sistema*/
#define MAGIC_NUMBER 890282536	/*los tres últimos dígitos de nuestros NIA en orden*/
#define DEVICE_IMAGE2 "disk.dat"/* Nombre del dispositivop predeterminado*/
#define MAX_FILE_NUMBER 50		/*Número máximo de ficheros soportado*/
#define FREE_FD 200 			/*Valor que indica una entrada libre la tabla de descriptores de fichero
								* Debe ponerse en el campo inodo de dicha entrada.
								*/
#define ANSI_FONT_BOLD   "\x1b[1m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_COLOR_RED     "\x1b[31m"

//ESTRUCTURAS DE DATOS
typedef struct{
	int maxNumFiles;					//número máximo de ficheros
	int numero_magico;
}SuperBloque;

typedef struct{
	char libre; //Será S si está libre y N si no lo está.
	uint8_t id;	//número del inodo
	char type;	//tipo de inodo, si es un fichero será 'f' y si es un directorio será 'd'
	uint8_t data_block; //número de bloque del bloque de datos que apunta. 
						//Los bloques de datos comienzan en 7+maxNumFiles+1 (datos del directorio raiz)
	char hashtag[3][HASHTAG_SIZE];
	//char hashtag1[HASHTAG_SIZE];
	//char hashtag2[HASHTAG_SIZE];
	//char hashtag3[HASHTAG_SIZE];
	char nombre [FILENAME_SIZE];
	uint16_t filesize; //tamaño del fichero en bytes

}Inodo; //Los i-nodos comienzan en el bloque 7 (raiz) y continúan hasta 7+maxNumFiles

typedef struct{
	char hashtag[HASHTAG_SIZE];
	uint8_t num_references;
}HashtagBlockEntry;

typedef struct{
	char nombre[FILENAME_SIZE];
	uint8_t inodo;
}DirEnt;

typedef struct{
	uint8_t inodo;		//Numero i-nodo
	uint16_t pos_pt; 	//Puntero lectura/escritura
}Fd_entry;

SuperBloque superbloque;
char global_i_map[BLOCK_SIZE];
char global_d_map[BLOCK_SIZE];
Inodo *inodos;
Fd_entry tabla_descriptores[MAX_FILE_NUMBER];
char bloqueDatosRaiz[BLOCK_SIZE];
HashtagBlockEntry etiquetas[HASHTAG_AMOUNT];

int mkFS(int maxNumFiles, long deviceSize) {
	if (maxNumFiles > MAX_FILE_NUMBER || maxNumFiles <=0 || deviceSize < 327680 || deviceSize > 512000){
		//Esto se produce si los parámetros son incorrectos.
		return -1;
	}
	//Escritura de la tabla de particiones
	char mbrBuffer[BLOCK_SIZE];
	char superbloqueBuffer[BLOCK_SIZE];
	char bytemap_inodos[BLOCK_SIZE];
	char bytemap_data[BLOCK_SIZE];
	char hashtag_block[BLOCK_SIZE];
	int ii;
	HashtagBlockEntry entrada_libre;
	Inodo inodoVacio;
															//Se reserva el espacio para los i-nodos
															/*inodos = (Inodo*) malloc(sizeof(Inodo)*(maxNumFiles+1));	*/
	
	bzero(mbrBuffer, BLOCK_SIZE);
	bwrite(DEVICE_IMAGE2, 0, mbrBuffer);

	//Escritura de los datos del superbloque
	SuperBloque superLopez;
	superLopez.maxNumFiles = maxNumFiles;
	superLopez.numero_magico = MAGIC_NUMBER; 		//y el número inodos es el mismo que el de ficheros más uno 
													//y el número de bloques de datos igual	
	
	bzero(superbloqueBuffer, BLOCK_SIZE);
	memcpy(superbloqueBuffer, &superLopez, sizeof(SuperBloque));
	bwrite(DEVICE_IMAGE2, 1, superbloqueBuffer);
	/*	Creamos el buffer del mapa de bytes de inodos.
	*	Lo inicializamos a 0 y ponemos su contenido en su sitio
	*	Y en el sitio de la copia correspondiente.
	*/

	bzero(bytemap_inodos, BLOCK_SIZE);
	//Se ocupa el i-nodo de la raíz
	bytemap_inodos[0] = 1;
	bwrite(DEVICE_IMAGE2, 2, bytemap_inodos); //Mapa de i-nodos original
	bwrite(DEVICE_IMAGE2, 3, bytemap_inodos); //Copia de mapa de i-nodos para soporte a fallos
	/*Lo mismo para el bytemap de bloques de datos*/
	bzero(bytemap_data, BLOCK_SIZE);
	//Se ocupa el bloque de datos de la raíz
	bytemap_data[0] = 1;
	bwrite(DEVICE_IMAGE2, 4, bytemap_data); //Mapa de bloques original
	bwrite(DEVICE_IMAGE2, 5, bytemap_data); //Copia de mapa de bloques para soporte a fallos

	//Se inicializa toda la tabla de etiquetas con valores por defecto.
	memset(entrada_libre.hashtag, 1, HASHTAG_SIZE);
	entrada_libre.num_references = 0;
	//hay que escribirlo en el buffer
	for(ii = 0; ii < HASHTAG_AMOUNT; ii++){
		memcpy(hashtag_block+(ii*sizeof(HashtagBlockEntry)), &entrada_libre, sizeof(HashtagBlockEntry));
	}
	bwrite(DEVICE_IMAGE2, 6, hashtag_block);


	//Hay que escribir tantos inodos como ficheros nos den más el inodo del directorio raíz
	Inodo nodoRaiz;
	nodoRaiz.libre = 'N';
	nodoRaiz.id = 0;
	nodoRaiz.type = 'd';
	nodoRaiz.data_block = (uint8_t)(1+maxNumFiles+7); //Tiene valor 1 para en las operaciones realizar 7+maxNumFiles+data_block
						 //Y así acceder directamente al bloque de datos. Si quereis lo podeis cambiar
	//Sin etiquetas porque es directorio
	
	strcpy(nodoRaiz.nombre, "/");
	nodoRaiz.filesize = sizeof(DirEnt)*3;
	char inodoBuffer[BLOCK_SIZE];
	bzero(inodoBuffer, BLOCK_SIZE);
	memcpy(inodoBuffer, &nodoRaiz, sizeof(Inodo));
	if(bwrite(DEVICE_IMAGE2, 7, inodoBuffer) == -1){
		printf("Errores horribles\n");
	}

	//Se genera el resto de i-nodos posibles. Todos libres y sólo deberían
	//ser ocupados por ficheros
	inodoVacio.libre = 'S';
	bzero(inodoBuffer, BLOCK_SIZE);
	for (ii = 1; ii < maxNumFiles; ii++){
		memcpy(inodoBuffer, &inodoVacio, sizeof(Inodo));
		bwrite(DEVICE_IMAGE2, 7+ii, inodoBuffer);
	}

	//Se crea el primer bloque de datos, el del directorio raíz.
	DirEnt directorio[3];
	/*  .   0
	    ..  0 */
	strcpy(directorio[0].nombre, ".");
	directorio[0].inodo = 0;
	strcpy(directorio[1].nombre, "..");
	directorio[1].inodo = 0;
	strcpy(directorio[2].nombre, "$");
	char bloqueDatos[BLOCK_SIZE];
	bzero(bloqueDatos, BLOCK_SIZE);
	memcpy(bloqueDatos, &directorio, sizeof(DirEnt)*3);
	bwrite(DEVICE_IMAGE2, 7+maxNumFiles+1, bloqueDatos);
	return 0;
}

/*
 * Mounts a file system from the device deviceName.
 * Returns 0 if the operation was correct or -1 in case of error.
 */
int mountFS() {

	//Leer el bloque 0 (superbloque)
	char bloque[BLOCK_SIZE];
	char aux[BLOCK_SIZE];
	bzero(bloque, BLOCK_SIZE);

	if (bread(DEVICE_IMAGE2, 1, bloque) == -1) return -1;
	memcpy(&(superbloque), bloque, sizeof(SuperBloque));

	if(superbloque.numero_magico != MAGIC_NUMBER){
		printf("%s%sERROR:%s Este sistema de ficheros no es del tipo que queremos\n", ANSI_COLOR_RED, ANSI_FONT_BOLD, ANSI_COLOR_RESET);
	}
	//Leer el mapa de i-nodos y la copia
	if (bread(DEVICE_IMAGE2, 2, global_i_map) == -1) return -1;
	if (bread(DEVICE_IMAGE2, 3, aux) == -1) return -1;
	if(strncmp(global_i_map, aux, BLOCK_SIZE) != 0){
		printf("%s%sERROR:%s Este sistema de ficheros tiene un problema con la integridad\n", ANSI_COLOR_RED, ANSI_FONT_BOLD, ANSI_COLOR_RESET);	
	}
	//Leer el mapa de datos
	if (bread(DEVICE_IMAGE2, 4, global_d_map) == -1) return -1;
	if (bread(DEVICE_IMAGE2, 5, aux) == -1) return -1;
	if(strncmp(global_d_map, aux, BLOCK_SIZE) != 0){
		printf("%s%sERROR:%s Este sistema de ficheros tiene un problema con la integridad\n", ANSI_COLOR_RED, ANSI_FONT_BOLD, ANSI_COLOR_RESET);	
	}
	//Se leen todos los i-nodos
	inodos = (Inodo *) malloc(sizeof(Inodo)*(superbloque.maxNumFiles+1)); //En el umount se realiza un free
	int i;
	for (i=0; i<superbloque.maxNumFiles+1;i++){
		bzero(bloque, BLOCK_SIZE);
		if ( bread(DEVICE_IMAGE2, 7+i, bloque) == -1) return -1;
		memcpy(&inodos[i], bloque, sizeof(Inodo));
	}

	bzero(bloqueDatosRaiz, BLOCK_SIZE);
	if (bread(DEVICE_IMAGE2, inodos[0].data_block, bloqueDatosRaiz) == -1) return -1;

	for (i=0; i<MAX_FILE_NUMBER;i++){
		tabla_descriptores[i].inodo = FREE_FD; //marcar como libre estas entradas en la tabla de desciptores ficheros
	}

	//Se pone en memoria el bloque de etiquetas
	char hashtag_block[BLOCK_SIZE];
	if (bread(DEVICE_IMAGE2, 6, hashtag_block) == -1) return -1;
	//etiquetas = (HashtagBlockEntry *) malloc(sizeof(HashtagBlockEntry)*30);
	for(i = 0; i < HASHTAG_AMOUNT; i++){
		memcpy(&etiquetas[i], hashtag_block+sizeof(HashtagBlockEntry)*i, sizeof(HashtagBlockEntry));
	}

	return 0;
}

/*
 * Unmount file system.
 * Returns 0 if the operation was correct or -1 in case of error.
 */
int umountFS() {
	char buffer[BLOCK_SIZE];
	//Escribir el bloque 0 (superbloque)
	bzero(buffer, BLOCK_SIZE);
	memcpy( buffer, (char*)&superbloque, sizeof(SuperBloque));
	if (bwrite(DEVICE_IMAGE2, 1, buffer) == -1)
		return -1;
	
	//Escribir el mapa de i-nodos
	if (bwrite(DEVICE_IMAGE2, 2, global_i_map) == -1)
		return -1;

	if (bwrite(DEVICE_IMAGE2, 3, global_i_map) == -1)
		return -1;

	//Escribir el mapa de datos
	if (bwrite(DEVICE_IMAGE2, 4, global_d_map) == -1)
		return -1;

	if (bwrite(DEVICE_IMAGE2, 5, global_d_map) == -1)
		return -1;

	//Se escriben todos los i-nodos
	int ii;
	for (ii=0; ii<superbloque.maxNumFiles+1;ii++){
		bzero(buffer, BLOCK_SIZE);	
		memcpy(buffer, (char *) &inodos[ii], sizeof(Inodo));
		if ( bwrite(DEVICE_IMAGE2, 7+ii, buffer) == -1 )
			return -1;
	}

	if (bwrite(DEVICE_IMAGE2, inodos[0].data_block, bloqueDatosRaiz) == -1)	
		return -1;	

	free(inodos);

	//Se copia al buffer el bloque de etiquetas para escribirlo
	bzero(buffer,BLOCK_SIZE);
	ii = 0;
	for(ii = 0; ii < HASHTAG_AMOUNT; ii++){
		memcpy(buffer+sizeof(HashtagBlockEntry)*ii, &etiquetas[ii], sizeof(HashtagBlockEntry));
	}
	if ( bwrite(DEVICE_IMAGE2, 6, buffer) == -1 )
			return -1;

	
	return 0;
}

/*******************/
/* File read/write */
/*******************/

/*
 * Creates a new file, if it doesn't exist.
 * Returns 0 if a new file is created, 1 if the file already exists or -1 in
 * case of error.
 */
int creatFS(char *fileName) {
	int ii = 0;
	int num_ficheros = 0;
	DirEnt current;
	bzero(&current, sizeof(current));
	DirEnt* ficheros;
	//Se verifica que el fichero sea de longitud menor o igual a la permitida
	while(fileName[ii] != '\0'){
		ii++;
	}
	if (ii > FILENAME_SIZE){ return -1; }

	ii = 0;
	if(strcmp(fileName, "$") == 0 || strcmp(fileName, ".") == 0 || strcmp(fileName, "..") == 0){
		printf("%s%sERROR:%s No se permiten los siguientes nombres de fichero\n «.»\n«..»\n«$»\n", ANSI_COLOR_RED, ANSI_FONT_BOLD, ANSI_COLOR_RESET);	
		return -1;
	}	
	while(strcmp(current.nombre, "$")){
		memcpy((char*)&current, bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
	
		if(strcmp(current.nombre, fileName) == 0){ //hemos encontrado el elemento
			return 1;
		}
		ii++;
	}
	//Si hemos llegado aquí, es que el nombre está disponible.
	num_ficheros = ii;
	if(num_ficheros-1 > superbloque.maxNumFiles){
		printf("%s%sERROR:%s No se pueden crear más ficheros en este sistema.\n", ANSI_COLOR_RED, ANSI_FONT_BOLD, ANSI_COLOR_RESET);	
		return -1;
	}
	ficheros = (DirEnt*)malloc(sizeof(DirEnt)*(num_ficheros+1));
	for(ii = 0; ii < num_ficheros; ii++){
		memcpy(&ficheros[ii], bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
	}

	//Copia el $ al final
	ficheros[num_ficheros] = ficheros[num_ficheros-1];
	//poner en ficheros el nombre del archivo
	strcpy(ficheros[num_ficheros-1].nombre, fileName);
	//poner el número de inodo en la entrada de directorio
	
	int num_inodo = 0;
	while (global_i_map[num_inodo] == 1) {	//Buscando en el mapa de bites un inodo libre
		num_inodo++;
	}

	ficheros[num_ficheros-1].inodo = num_inodo;
	//Se actualiza en memoria el bloque de datos de la raiz
	memcpy(bloqueDatosRaiz, ficheros, sizeof(DirEnt)*(num_ficheros+1) );

	int num_data = 0;
	while (global_d_map[num_data] == 1) {
		num_data++;
	}//busco un bloque de datos libre en el bitmap
	global_i_map[num_inodo] = 1;
	inodos[num_inodo].libre = 'N';
	inodos[num_inodo].id = num_inodo;
	inodos[num_inodo].type = 'f';
	inodos[num_inodo].data_block = num_data + superbloque.maxNumFiles + 1 + 7;
	inodos[num_inodo].filesize = 0;
	memset(inodos[num_inodo].hashtag[0], 1, HASHTAG_SIZE);
	memset(inodos[num_inodo].hashtag[1], 1, HASHTAG_SIZE);
	memset(inodos[num_inodo].hashtag[2], 1, HASHTAG_SIZE);
	strcpy(inodos[num_inodo].nombre, fileName);

	global_d_map[num_inodo] = 1;
	char datos[BLOCK_SIZE];
	bzero(datos, BLOCK_SIZE);
	bwrite(DEVICE_IMAGE2, inodos[num_inodo].data_block, datos);

	////////////////// FALTAN LOS RETURN -1
	free(ficheros);
	return 0;
}

/*
 * Opens an existing file.
 * Returns file descriptor if possible, -1 if file does not exist or -1 in case
 * of error.
 */
int openFS(char *fileName) {
	//	Para hacer esto hay que:
	//		Comprobar si ese fichero existe
	//		Comprobar si hay un descriptor de fichero libre.
	//		Si el fichero existe, rellenar la tabla con los datos del fichero
	//		Devolver el descriptor de fichero.
	DirEnt current;
	strcpy(current.nombre, "Galletas");
	DirEnt* ficheros;
	uint8_t inodo_fichero_abierto = 0; //imposible que este valor sea correcto, el del inodo raíz.
	int ii = 0;
	int num_ficheros;
	//////////////////////////////////////////////////////////////////////////////////////////
	while(strcmp(current.nombre, "$")){
		memcpy((char*)&current, bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
		ii++;
	}

	num_ficheros = ii;
	ficheros = (DirEnt*)malloc(sizeof(DirEnt)*(num_ficheros));
	for(ii = 0; ii < num_ficheros; ii++){
		memcpy(&ficheros[ii], bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
	}

	//Ya tenemos en 'ficheros' los ficheros contenidos en el raíz.
	//Debemos buscar en ellos el fichero pedido.
	for(ii = 0; ii < num_ficheros; ii++){
		if(strcmp(fileName, ficheros[ii].nombre) == 0){ //bingo
			inodo_fichero_abierto = ficheros[ii].inodo; //guardamos el número del inodo que ocupa el archivo.
			if(strcmp(inodos[inodo_fichero_abierto].nombre, ficheros[ii].nombre) !=0 ){//Comprobación de integridad
				return -1;
			}

		}
	}
	if(inodo_fichero_abierto == 0){ //si se ha recorrido a lista sin encontrar el fichero.
		return -1;
	}
	//A estas alturas sabemos que el fichero exite y su número de inodo.
	//Debemos buscar un descriptor de fichero libre.
	for(ii = 0; ii < MAX_FILE_NUMBER; ii++){
		if(tabla_descriptores[ii].inodo == FREE_FD){//descriptor libre
			tabla_descriptores[ii].inodo = inodo_fichero_abierto;
			tabla_descriptores[ii].pos_pt = 0;//se sitúa la cabeza lectora/escritora en el inicio del fichero.

			free(ficheros);//esta línea me ha llevado varias horas, y provocaba errores bizantinos...
			return ii;
		}
	}

	//No existe descriptores libres;
	return -1;
}

/*
 * Closes a file.
 * Returns 0 if the operation was correct or -1 in case of error.
 */
int closeFS(int fileDescriptor) {
	if (fileDescriptor < 0 || fileDescriptor > MAX_FILE_NUMBER) return -1;

	if(tabla_descriptores[fileDescriptor].inodo != FREE_FD){
		tabla_descriptores[fileDescriptor].inodo = FREE_FD; //Se libera el descriptor
		tabla_descriptores[fileDescriptor].pos_pt = 0;//se resetea el puntero por si acaso.
		return 0;
	}
	//No se encuentra el descriptor
	return -1;
}

/*
 * Reads a number of bytes from a file and stores them in a buffer.
 * Returns the number of bytes read or -1 in case of error.
 */
int readFS(int fileDescriptor, void *buffer, int numBytes) {
	// Para hacer esto hay que buscar el descriptor de fichero en la tabla
	// Si no existe, se reotrna error.
	// Si por el contrario sí existe, cargamos los datos de su inodo
	// Calcular puntero+numBytes y leer ese tamaño y devolverlo.
	int bytesDisponibles = 0, ii;
	if(fileDescriptor >= MAX_FILE_NUMBER || fileDescriptor < 0){
		return -1;
	}
	if(tabla_descriptores[fileDescriptor].inodo == FREE_FD){ //si ese descriptor está libre
		return -1;
	}
	Inodo inodoFicheroLeido = inodos[tabla_descriptores[fileDescriptor].inodo];
	//Calculo cuántos bytes puedo leer.
	bytesDisponibles = inodoFicheroLeido.filesize - (tabla_descriptores[fileDescriptor].pos_pt);
	//esto en teoría nunca podría darse si el sistema permanece íntegro.
	if(bytesDisponibles < 0){
		return -1;
	}
	//Si estamos al final (posición == a logitud fichero) no podemos leer, pero no es error.
	if(bytesDisponibles == 0){
		return 0;
	}
	//debo cargar su bloque de datos.
	char block[BLOCK_SIZE];
	bread(DEVICE_IMAGE2, inodoFicheroLeido.data_block, block);
	for(ii = 0; (ii < bytesDisponibles) && (ii < numBytes); ii++){
		*((char*)(buffer+ii)) = block[ii+tabla_descriptores[fileDescriptor].pos_pt];
	}
	tabla_descriptores[fileDescriptor].pos_pt += (ii);
	return (ii);
}

/*
 * Reads number of bytes from a buffer and writes them in a file.
 * Returns the number of bytes written, 0 in case of end of file or -1 in case
 * of error.
 */
int writeFS(int fileDescriptor, void *buffer, int numBytes) {
	int  ii;
	if(fileDescriptor >= MAX_FILE_NUMBER || fileDescriptor < 0){
		return -1;
	}
	if(tabla_descriptores[fileDescriptor].inodo == FREE_FD){ //si ese descriptor está libre
		return -1;
	}
	uint8_t id_inodo_fichero_escrito = tabla_descriptores[fileDescriptor].inodo;
	//Calculo el nuevo tamaño del fichero
	int aumento_longitud = tabla_descriptores[fileDescriptor].pos_pt + numBytes - inodos[id_inodo_fichero_escrito].filesize;
	if(aumento_longitud <= 0){
		aumento_longitud = 0;
	}
	if(inodos[id_inodo_fichero_escrito].filesize + aumento_longitud > BLOCK_SIZE){
		inodos[id_inodo_fichero_escrito].filesize = BLOCK_SIZE;
	}else{
		inodos[id_inodo_fichero_escrito].filesize+= aumento_longitud;
	}
	//calculamos cuántos bytes podremos escribir:
	
	//traemos el bloque del fichero a memoria
	char block[BLOCK_SIZE];
	bread(DEVICE_IMAGE2, inodos[id_inodo_fichero_escrito].data_block, block);
	//renombramiento
	int puntero = tabla_descriptores[fileDescriptor].pos_pt;
	int filesize = inodos[id_inodo_fichero_escrito].filesize;
	//modificamos dicho bloque que está en memoria
	for(ii = 0; (ii < numBytes) && (ii+puntero < filesize); ii++){
		block[ii+puntero] = *(((char*) buffer)+ii);
	}
	bwrite(DEVICE_IMAGE2, inodos[id_inodo_fichero_escrito].data_block, block);
	tabla_descriptores[fileDescriptor].pos_pt += (ii);
	return ii;

}


/*
 * Repositions the pointer of a file. A greater offset than the current size, or
 * an offset lower than 0 are considered errors.
 * Returns new position or -1 in case of error.
 */
int lseekFS(int fileDescriptor, long offset, int whence) {
	if(fileDescriptor >= MAX_FILE_NUMBER){
		return -1;
	}
	if(tabla_descriptores[fileDescriptor].inodo == FREE_FD){
		printf("%s%sERROR:%sArchivo no abierto.\n", ANSI_COLOR_RED, ANSI_FONT_BOLD, ANSI_COLOR_RESET);
		return -1;
	}
	int filesize = inodos[tabla_descriptores[fileDescriptor].inodo].filesize;
	int puntero = tabla_descriptores[fileDescriptor].pos_pt;
	switch(whence){
		case FS_SEEK_SET://posición actual.
			if(( puntero + offset >= filesize) || (puntero + offset < 0) ){
				printf("%s%sERROR:%s Has intentado hacer lseek fuera del fichero\n", ANSI_FONT_BOLD, ANSI_COLOR_RED, ANSI_COLOR_RESET );
				return -1;
			}
			tabla_descriptores[fileDescriptor].pos_pt = offset + puntero;
			break;
		case FS_SEEK_BEGIN: //principio del fichero.			
			tabla_descriptores[fileDescriptor].pos_pt = 0;
			break;
		case FS_SEEK_END: //final del fichero.
			tabla_descriptores[fileDescriptor].pos_pt = filesize;
			break;
		default: 
			printf("%s%sERROR:%s argumento 'whence' de lseekFS incorrecto.\n",ANSI_FONT_BOLD, ANSI_COLOR_RED, ANSI_COLOR_RESET );
			return -1;
	}//fin switch


	return 0;
}

/**********************/
/* Version management */
/**********************/

/*
 * Tags a file with the given tag name. Returns 0 if the operation is
 * successful, 1 if the file already had that tag or -1 in case of error.
 */
int tagFS(char *fileName, char *tagName) {
	int ii = 0;
	int num_ficheros = 0;
	DirEnt current;
	bzero(&current, sizeof(current));
	DirEnt* ficheros;

	//Se verifica que la etiqueta sea de longitud menor o igual a la permitida
	while(tagName[ii] != '\0'){
		ii++;
	}
	if (ii > HASHTAG_SIZE){ return -1; }

	ii = 0;
	//Se recuperan todos los ficheros
	while(strcmp(current.nombre, "$")){
		memcpy((char*)&current, bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
		ii++;
	}
	num_ficheros = ii;

	//Se recuperan los ficheros en una estructura que se pueda utilizar
	ficheros = (DirEnt*)malloc(sizeof(DirEnt)*num_ficheros);

	for(ii = 0; ii < num_ficheros; ii++){
		memcpy(&ficheros[ii], bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
	}

	ii = 0;
	while(strcmp(ficheros[ii].nombre, fileName) != 0){
		//Se ha recorrido todo el directorio sin encontrar el archivo
		if (strcmp(ficheros[ii].nombre, "$") == 0){
			free(ficheros);
			return -1;
		}
		ii++;
	}
	/*La posición del fichero es la ii.
	Se verifican las etiquetas que tiene el archivo.*/
	int inodoFichero = ficheros[ii].inodo;

	// Antes de continuar hay que verificar que el fichero está cerrado
	for (ii=0; ii < MAX_FILE_NUMBER; ii++){
		if (tabla_descriptores[ii].inodo == inodoFichero){
			//El fichero está abierto
			free(ficheros);
			return -1;
		}
	}

	/*Se comprueba con las etiquetas que existen en el archivo. Si ya posee la etiqueta
	se devuelve 1 */
	for (ii=0; ii<3; ii++){
		if (strcmp(inodos[inodoFichero].hashtag[ii],tagName) == 0) {
			free(ficheros);
			return 1;
		}
	}
	int etiquetas_totales = 0;
	for(ii = 0; ii < HASHTAG_AMOUNT; ii++){
		if(etiquetas[ii].num_references != 0){
			etiquetas_totales ++;
		}
	}
	if(etiquetas_totales >= HASHTAG_AMOUNT){
		return -1;
	}

	for (ii=0; ii<3; ii++){
		//Se busca la primera etiqueta libre
		int w = 0;
		while (inodos[inodoFichero].hashtag[ii][w] == 1 && w<HASHTAG_SIZE){ w++; }
		//La etiqueta ii está libre
		if (w == HASHTAG_SIZE){
			//Primero hay que comprobar si la etiqueta existía previamente, porque si no
			//existía y ya existen 30 etiquetas no se puede crear.
			for (w = 0; w<HASHTAG_AMOUNT; w++){
				//Si ya existía se actualiza y se guarda la etiqueta en el fichero
				if (strcmp(etiquetas[w].hashtag,tagName) == 0){
					etiquetas[w].num_references++;
					strcpy(inodos[inodoFichero].hashtag[ii], tagName);
					free(ficheros);
					return 0;
				}
			}

			//Si la etiqueta no existía previamente hay que buscar un hueco libre
			//y de existir un hueco, hay que añadirla a la tabla
			for (w = 0; w<HASHTAG_AMOUNT; w++){
				int s = 0;

				while (etiquetas[w].hashtag[s] == 1 && s<HASHTAG_SIZE) {
					//Se verifica si hay etiqueta libre.
					s++;
				}

				//Si la posición está libre, se escribe la nueva etiqueta con el nombre
				// y la única referencia que tiene
				if (s == HASHTAG_SIZE){
					strcpy(etiquetas[w].hashtag, tagName);
					etiquetas[w].num_references = 1;
					strcpy(inodos[inodoFichero].hashtag[ii], tagName);
					free(ficheros);
					return 0;
				} 
			}
			//si se llega aqui, no hay etiquetas libres en el bloque de etiquetas
			free(ficheros);
			return -1;
		}
	}

	//No hay etiquetas libres
	return -1;
}

/*
 * Removes a tag from a file. Returns 0 if the operation is successful, 1 if
 * the tag wasn't associated to the file or -1 in case of error.
 */
int untagFS(char *fileName, char *tagName) {
	int ii = 0;
	int num_ficheros = 0;
	DirEnt current;
	bzero(&current, sizeof(current));
	DirEnt* ficheros;

	//Se recuperan todos los ficheros
	while(strcmp(current.nombre, "$")){
		memcpy((char*)&current, bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
		ii++;
	}
	num_ficheros = ii;

	//Se recuperan los ficheros en una estructura que se pueda utilizar
	ficheros = (DirEnt*)malloc(sizeof(DirEnt)*num_ficheros);
	for(ii = 0; ii < num_ficheros; ii++){
		memcpy(&ficheros[ii], bloqueDatosRaiz+sizeof(DirEnt)*ii, sizeof(DirEnt));
	}

	ii = 0;
	while(strcmp(ficheros[ii].nombre, fileName) != 0){
		//Se ha recorrido todo el directorio sin encontrar el archivo
		if (strcmp(ficheros[ii].nombre, "$") == 0){
			free(ficheros);
			return -1;
		}
		ii++;
	}

	/*La posición del fichero es la ii.
	Se verifican las etiquetas que tiene el archivo.*/
	int inodoFichero = ficheros[ii].inodo;

	// Antes de continuar hay que verificar que el fichero está cerrado
	ii = 0;
	for (ii=0; ii < MAX_FILE_NUMBER; ii++){
		if (tabla_descriptores[ii].inodo == inodoFichero){
			//El fichero está abierto
			free(ficheros);
			return -1;
		}
	}

	/*Se comprueba con las etiquetas que existen en el archivo. Si no posee la etiqueta
	se devuelve 1 */
	if ( strcmp(inodos[inodoFichero].hashtag[0],tagName) != 0 &&
		 strcmp(inodos[inodoFichero].hashtag[1],tagName) != 0 &&
		 strcmp(inodos[inodoFichero].hashtag[2],tagName) != 0) { free(ficheros); return 1; }

	for (ii=0; ii<3; ii++){
		if (strcmp(inodos[inodoFichero].hashtag[ii],tagName) == 0) {
			//Se libera la entrada del inodo
			memset(inodos[inodoFichero].hashtag[ii], 1, HASHTAG_SIZE);
			int j = 0;
			for (j = 0; j<HASHTAG_AMOUNT; j++){
				if (strcmp(etiquetas[j].hashtag,tagName) == 0) {
					etiquetas[j].num_references--;
					//Si no hay más ficheros con la etiqueta debe borrarse
					if (etiquetas[j].num_references == 0) {
						memset(etiquetas[j].hashtag, 1, HASHTAG_SIZE);
					}
					free(ficheros);
					return 0;
				}
			}
		}
	}

	//No se debería llegar, ya que si el fichero tiene la etiqueta debería de existir
	//en el bloque de etiquetas;
	free(ficheros);
	return -1;
}

/*
 * Looks for all files tagged with the tag tagName, and stores them in the list
 * passed as a parameter. Returns the number of tagged files found or -1 in
 * case of error.
 */
int listFS(char *tagName, char **files) {
	/*	Buscar en el bloque de etiquetas si está dicha etiqueta
	*	Si no está, devolvemos nulo.
	*	Si está, buscamos los inodos en los que se encuentre.
	*	Reservamos memoria de file.
	*	Ponemos los nombres.
	*/
	int ii = 0, jj = 0;
	int num_references = 0;
	int bingos = 0;
	for(ii = 0; ii < HASHTAG_SIZE; ii++){
		if(tagName[ii] == '\0'){ //Hemos llegado al final
			break;
		}
		if(ii == HASHTAG_SIZE-1){//Hemos llegado al final sin encontrar el carácter terminador.
			printf("%s%sERROR listFS: %sEtiqueta muy larga, revise las entradas\n", ANSI_FONT_BOLD, ANSI_COLOR_RED, ANSI_COLOR_RESET);
			return -1;
		}
	}
	for(ii = 0; ii<HASHTAG_AMOUNT; ii++){
		if(strcmp(etiquetas[ii].hashtag, tagName) == 0){
			num_references = etiquetas[ii].num_references;
		}
	}
	if(num_references == 0){ //etiqueta no encontrada.		
		return 0;
	}
	//Si hemos llegado aquí, sabemos que la etiqueta sí existe.
	//ATENCIÓN: SUPONGO QUE LA MEMORIA ESTÁ RESERVADA
	//encontramos los nombres de archivo que están etiquetados así:
	ii = 0;
	while(bingos < num_references){//Hasta que hagamos tantas coincidencias como el número de referencias.
		for(jj = 0; jj < 3; jj++){//para todas las etiquetas de un inodo, comprobamos y son la requerida
			if(strncmp(inodos[ii].hashtag[jj], tagName, strlen(tagName)) == 0){//si es, se copia en la posición correspondiente.
				strcpy(files[bingos], inodos[ii].nombre);
				bingos++;
			}
		}
		ii++;
	}
	return num_references;
}

