
/*
 *  disk_driver.c
 *
 *  DrvSim. Version 2.0
 *
 *  INFODSO@ARCOS.INF.UC3M.ES
 *
 */

#include "disk_driver.h"
#include <pthread.h>


/* 
 * Elements available within pKernel
 */

int send_data_to_process   ( pid_t p_id, int operation, int block_id, char *block, int error ) ;
int request_data_to_device (             int operation, int block_id, char *block, int error ) ;
int is_disk_processing     ( void ) ;


/*
 * Elements to be implemented
 */


//Cola de peticiones del disco
struct queue* peticiones;
request* orden66; //petición que el disco está procesando ahora mismo.

//Mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int mount_driver ( void ){
    // TODO
    peticiones = queue_new();
    if (peticiones!=NULL) return 1;
    return -1;
}

int unmount_driver ( void ){
    queue_destroy(peticiones);
    pthread_mutex_destroy(&mutex);
    return 1;   
}


int disk_driver_block_request ( pid_t p_id, int operation, int block_id, char *block, int error){
    pthread_mutex_lock(&mutex);
    int ii = 0;
    //buscar la operación más reciente del mismo bloque
    request* petmasrecent = buscar_por_atras(peticiones, block_id, orden66);

    //modificaciones de las estructuras de datos.
    if (petmasrecent == NULL) { // no había más peticiones para este bloque
        //creo una petición.
        request pet;
        pet.operation = operation;
        pet.block_id = block_id;
        for (ii = 0; ii < BLOCK_SIZE; ++ii){
            pet.block[ii] = block[ii];
        }
        pet.error = error;
        pet.aforo = 0;
        //encolo esta subpetición inicial en la cola.
        encolar_subpeticion(&pet, block, p_id);
        //encolo en la cola la petición, 
        //se hará una copia así que no importa que sea variable global
        enqueue_ordered(peticiones, &pet);
            //TRAZA. Cuando se reciba una petición y se encole una nueva petición asociada:
            printf("\tDRIVER: request o-enqueued, pid: %i, block_id: %i\n", p_id, block_id);
    }
    else if(operation == petmasrecent->operation){ //Si la operación es igual
        if(is_pet_full(petmasrecent)){//si no puedo encolar
                //creo una petición.
                request pet;
                pet.operation = operation;
                pet.block_id = block_id;
                for (ii = 0; ii < BLOCK_SIZE; ++ii){
                    pet.block[ii] = block[ii];
                }
                pet.error = error;
                pet.aforo = 0;
                //encolo esta subpetición inicial en la cola.
                encolar_subpeticion(&pet, block, p_id);
                //encolo en la cola la petición, 
                //se hará una copia así que no importa que sea variable global
                enqueue_ordered(peticiones, &pet);
                    //TRAZA. Cuando se reciba una petición y se encole una nueva petición asociada:
                    printf("\tDRIVER: request o-enqueued, pid: %i, block_id: %i\n", p_id, block_id);
        }else{//Si no esta llena
            encolar_subpeticion(petmasrecent, block, p_id);
                //TRAZA. Cuando se reciba una petición y se encole en una petición existente:
                printf("\tDRIVER: request re-enqueued, pid: %i, block_id: %i\n", p_id, block_id);

        }
    }
    else if (operation != petmasrecent->operation){ //Si la operación es distinta
        //creo una petición.
        request pet;
        pet.operation = operation;
        pet.block_id = block_id;
        for (ii = 0; ii < BLOCK_SIZE; ++ii){
            pet.block[ii] = block[ii];
        }
        pet.error = error;
        pet.aforo = 0;
        //encolo esta subpetición inicial en la petición normal.
        encolar_subpeticion(&pet, block, p_id);
        //encolo en la cola la petición, 
        //se hará una copia así que no importa que sea variable local
        enqueue_ordered(peticiones, &pet);
            //TRAZA. Cuando se reciba una petición y se encole una nueva petición asociada:
            printf("\tDRIVER: request o-enqueued, pid: %i, block_id: %i\n", p_id, block_id);
    }

    //ahora que ya hemos modificado las estructuras de datos empezamos a comunicarnos con el disco
    if(is_disk_processing() == 1){ //si el disco está trabajando
        pthread_mutex_unlock(&mutex);
        return 1;
    }
    //si el disco está, por el contrario, libre. Pedirle que empiece con la primera petición
    orden66 = &(peticiones->head->req);    
    request_data_to_device(orden66->operation, orden66->block_id, orden66->block, orden66->error);
        //TRAZA. Cuando el dispositivo esté libre y se envíe al dispositivo:
        printf("\tDRIVER: request sent, pid: %i, block_id: %i\n", p_id, block_id);
    pthread_mutex_unlock(&mutex);
    return 1;
}

int disk_driver_hardware_interrupt_handler ( pid_t p_id, int operation, int block_id, char *block, int error){
    pthread_mutex_lock(&mutex);
        int ii = 0;
        
       /* //Debemos identificar qué petición de la cola ha atendido el disco
        request* petAtendida = find_request(peticiones, block_id, operation);*/
        //conseguimos la siguiente peticion
        request* nextPet = next_request(peticiones, orden66);
        //notificamos a los procesos que se ha atendido su petición    
        for(ii = 0; ii < orden66->aforo; ii++){
            //se devuelve el block de argumento porque confiamos ciegamenete ya que no hay especificacion de la interfaz.
            send_data_to_process(orden66->pid[ii], orden66->operation, orden66->block_id, block, orden66->error );
                //TRAZA. Cuando se envíe una petición correcta a un cliente, se imprimirá:
                printf("\tDRIVER: sending data, pid: %i, block_id: %i\n", p_id, block_id);
        }
        //eliminamos la petición atendida de la cola.
        remove_request (peticiones, orden66 );
            //TRAZA. Cuando se desencole la petición:
            printf("\tDRIVER: request dequeued, block_id: %i\n", block_id);
        if(is_queue_empty(peticiones)){ //si la cola está vacía
            pthread_mutex_unlock(&mutex);
            return 1;
        }
        //si hay más peticiones, hay que encargar que se haga la siguiente petición
        orden66 = nextPet;
        request_data_to_device(orden66->operation,  orden66->block_id, orden66->block, orden66->error);
            //TRAZA. Cuando existan peticiones pendientes en la cola y se envíen al dispositivo:
            printf("\tDRIVER: pending sent, block_id: %i\n", orden66->block_id); 

    pthread_mutex_unlock(&mutex);
    return 1;
}