#ifndef _QUEUE_H_
#define _QUEUE_H_

	/* 
         * Includes
         */
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
    #define BLOCK_SIZE 1024
    #define WRITE 1
    #define READ 0
	#include "link.h"
	/* 
     * Datatypes 
     */
	typedef struct request{		
             int  operation;
             int  block_id;
             char block[BLOCK_SIZE];
             
             /* TODO: ADD YOUR OWN REQUEST STRUCTURE FIELDS */
             //Para la primera parte basta con este pid, pero para más adelante
          	 //hay que crear una lista de pids y cambiar esto en el driver.
             pid_t pid[10]; //pid de los procesos que han pedido bloque, se empieza a asignar por la posición 0, y se sigue por la 1, la 2...
             int error;
             int aforo; 
    } request;

	struct queue_element
	{
	  request req;
	  struct queue_element* next;
	  struct queue_element* prev;
	};

	struct queue
	{
	  struct queue_element* head;
	  struct queue_element* tail;
	};

	/* 
     * Functions 
     */

	/* Create an empty queue */
	struct queue* queue_new ( void );
	/* Destroy a previously created queue */ 
	void queue_destroy ( struct queue *q );
	/* Enqueue an element */
	struct queue* enqueue ( struct queue* s, request * req );
	/* Try to enqueue an element in an ordered fashion */
	struct queue* enqueue_ordered ( struct queue* s, request * req );
	/* Remove an element */
	int remove_request ( struct queue* s, request * req );
	/* To find an existing request */
	request * find_request ( struct queue* s, int block_id, int operation); 
	/* Get the next element, or return the first one in the queue */
	request * next_request ( struct queue* s, request * req );
	/* Return 1 if the queue is empty and 0 otherwise */
	int is_queue_empty ( struct queue* s );
	/*Imprime la cola*/
	void print(struct queue* s);
	/*returns the oldest element in the queue and deletes it from the queue*/
	request dequeue(struct queue* c);
	/*Básicamente añade el pid pasado a la lista y sube el número de elementos*/
	int encolar_subpeticion(request* pet, char* buffer, pid_t pid);//añadida por paco
	/*Busca por id de bloque y por código de operación la petición desde el final de la cola.*/
	request* buscar_por_atras(struct queue* cola, int block_id, request* orden66);
	/*dice si la petición está llena, en ese caso devuelve 1, en el resto, 0*/
	int is_pet_full(request* pet);

#endif

