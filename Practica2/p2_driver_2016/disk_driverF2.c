
/*
*  disk_driver.c
*
*  DrvSim. Version 2.0
*
*  INFODSO@ARCOS.INF.UC3M.ES
*
*/

#include "disk_driver.h"
#include "queue.h"

/* 
* Elements available within pKernel
*/

int send_data_to_process ( pid_t p_id, int operation, int block_id, char *block, int error );
int request_data_to_device ( int operation, int block_id, char *block, int error );
int is_disk_processing ( void );


/*
* Elements to be implemented
*/

//Cola de peticiones del disco
struct queue* peticiones;

//Mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int mount_driver ( void ){
    // TODO
    peticiones = queue_new();
    if (peticiones!=NULL) return 1;
    return -1;
}

int unmount_driver ( void ){
    queue_destroy(peticiones);
    pthread_mutex_destroy(&mutex);
    return 1;	
}


//llega una nueva petición
int disk_driver_block_request ( pid_t p_id, int operation, int block_id, char *block, int error ){
	pthread_mutex_lock(&mutex);
	int i;
	
	//creamos una petición
	request* pet = (request*) malloc(sizeof(request));
	pet->operation = operation;
	pet->block_id = block_id;
	for (i = 0; i < BLOCK_SIZE; ++i){
            pet->block[i] = block[i];
        }
    
	pet->pid[0] = p_id;
	pet->error = error;
	//Encolamos la petición
	enqueue_ordered(peticiones, pet);
        //TRAZA. Cuando se reciba una petición del cliente y se encole:
        printf("\tDRIVER: request o-enqueued, pid: %i, block_id: %i\n", p_id, block_id);	
	if(is_disk_processing() == 1){ //si el disco está ocupado
		pthread_mutex_unlock(&mutex);
		return 1;
	}//si el disco está libre, se le pide que empiece con esta petición.
	request_data_to_device (pet->operation, pet->block_id, pet->block, pet->error );
	    //TRAZA. Cuando el dispositivo esté libre y se envíe al dispositivo:
        printf("\tDRIVER: request sent, pid: %i, block_id: %i\n", p_id, block_id);
	pthread_mutex_unlock(&mutex);
	return 1;
}

//El bloque está listo para ser mandado al cliente
int disk_driver_hardware_interrupt_handler ( pid_t p_id, int operation, int block_id, char *block, int error ){

	pthread_mutex_lock(&mutex);
	request* current= find_request (peticiones, block_id, operation);
	//buscamos la siguiente
	request* nextpet = next_request(peticiones, current);
	//aquí mandamos al proceso los datos que me han puesto en el buffer que indiqué al hacer la petición.
	//se devuelve el block de argumento porque confiamos ciegamenete ya que no hay especificacion de la interfaz.

	send_data_to_process( current->pid[0], current->operation, current->block_id, block, current->error );
		//TRAZA. Cuando se envíe al cliente:
        printf("\tDRIVER: sending data, pid: %i, block_id: %i\n", p_id, block_id);
	//podemos eliminar la petición atendida
	remove_request (peticiones, current); current = NULL;
		//TRAZA. Cuando se desencole la petición:
        printf("\tDRIVER: request dequeued, block_id: %i\n", block_id);

	if(is_queue_empty(peticiones)){//si la cola está vacía
		pthread_mutex_unlock(&mutex);
		return 1; //FIN
	}
	//si por el contrario aún quedan peticiones.
	request_data_to_device (nextpet->operation, nextpet->block_id, nextpet->block, nextpet->error );
        //TRAZA. Cuando existan peticiones pendientes en la cola y se envíen al dispositivo:
        printf("\tDRIVER: pending sent, block_id: %i\n", nextpet->block_id);	
	pthread_mutex_unlock(&mutex);
	return 1;
}
