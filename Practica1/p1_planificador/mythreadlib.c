#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>
#include <ucontext.h>
#include <unistd.h>
#include "queue.h"
#include "mythread.h"
#include "interrupt.h"


TCB* ejecutando;
struct queue* esperando_rr;
struct queue* esperando_fifo;

TCB* scheduler();

void activator();

void timer_interrupt(int sig);

/* Array of state thread control blocks: the process allows a maximum of N threads */
static TCB t_state[N]; 

/* Variable indicating if the library is initialized (init == 1) or not (init == 0) */
static int init=0;

/* Initialize the thread library */
void init_mythreadlib() {
 // disable_interrupt();
  esperando_rr = queue_new();
  esperando_fifo = queue_new();
 // enable_interrupt();
  int i;
  t_state[0].state = INIT;
  t_state[0].tid = 0;
  t_state[0].priority = LOW_PRIORITY;
  t_state[0].ticks = QUANTUM_TICKS;
  t_state[0].hungry = STARVATION;
  if(getcontext(&t_state[0].run_env) == -1){
    perror("getcontext in my_thread_create");
    exit(5);
  }
  for(i=1; i<N; i++){
    t_state[i].state = FREE;
  }
  ejecutando = &t_state[0];//ponemos en ejecutando el primer proceso.
  init_interrupt();
}


/* Create and intialize a new thread with body fun_addr and one integer argument */ 
int mythread_create (void (*fun_addr)(),int priority){

  int i;
  
  if (!init) { init_mythreadlib(); init=1;}
  for (i=0; i<N; i++)
    if (t_state[i].state == FREE) break;
  if (i == N) return(-1);
  if(getcontext(&t_state[i].run_env) == -1){
    perror("getcontext in my_thread_create");
    exit(-1);
  }
  //Se asignan los atributos principales al TCB del hilo recien creado
  t_state[i].state = INIT;
  t_state[i].tid = i;
  t_state[i].ticks = QUANTUM_TICKS;
  t_state[i].hungry = STARVATION;
  t_state[i].priority = priority;
  t_state[i].function = fun_addr;
  t_state[i].run_env.uc_stack.ss_sp = (void *)(malloc(STACKSIZE));
  if(t_state[i].run_env.uc_stack.ss_sp == NULL){
    printf("thread failed to get stack space\n");
    exit(-1);
  }
  t_state[i].run_env.uc_stack.ss_size = STACKSIZE;
  t_state[i].run_env.uc_stack.ss_flags = 0;
  makecontext(&t_state[i].run_env, fun_addr, 1);  
 
  if(t_state[i].priority!=HIGH_PRIORITY){ //el proceso nunca provocará expulsión, es de baja (o media) prioridad.
    disable_interrupt();
    enqueue(esperando_rr, &t_state[i]); //Se ha creado un proceso pringadillo.
    enable_interrupt();
  }
  else{ //<--- se crea un proceso de alta prioridad
    if(ejecutando->priority == HIGH_PRIORITY){ //ya hay un proceso de alta prioridad ejecutando. (se encola)
      disable_interrupt();
      enqueue(esperando_fifo, &t_state[i]);            
      enable_interrupt();
    }
    else{//hay una expulsión porque había uno de RR ejecutando. 
      ejecutando->ticks = QUANTUM_TICKS;
      TCB* old = ejecutando; //el proceso de prioridad baja en ejecución
      TCB* next = &t_state[i]; //el proceso en prioridad alta que lo ha de expulsar
      ejecutando = next;
      disable_interrupt();
      enqueue(esperando_rr, old);
      enable_interrupt();
      activator(old, next);
    }//Aquí antes se contemplaba que el proceso ejecutara su rodaja para solucionar su inanición sin ser expulsado, pero sí que debe serlo.
  }
  


  return i;
} /****** End my_thread_create() ******/


/* Free terminated thread and exits */
void mythread_exit(){
  //int tid = mythread_gettid();	
   if (!init){ init_mythreadlib(); init=1;}
  //printf("Ha terminado un hilo con prioridad: %d\n", ejecutando->priority);

  ejecutando->state = FREE;

  //Se libera memoria
  //free(t_state[tid].run_env.uc_stack.ss_sp); ////////// AAAA ACHUGUENÑAAAAA CHIBABABALAAAAA ADAUDSGADADASD
  free(ejecutando->run_env.uc_stack.ss_sp);

  TCB* next = scheduler();
  if(next != NULL) { //En caso de que cualquier proceso empiece a ejecutar, debe resetearse su valor de 'hungry'
    next->hungry = STARVATION;
  }
 // printf("El estado del proceso ejecutando se ha cambiado\n");
  printf("*** THREAD %d FINISHED\n", ejecutando->tid);
  if(next != NULL) printf("*** THREAD %d TERMINATED : SET CONTEXT OF %d\n", ejecutando->tid,next->tid);
  ejecutando = next;
  activator(NULL, next);//llamamos al activator indicándole que no tiene que guardar el contexto actual.
}

/* Sets the priority of the calling thread */
void mythread_setpriority(int priority) {
  int tid = mythread_gettid();	
  t_state[tid].priority = priority;
}

/* Returns the priority of the calling thread */
int mythread_getpriority(int priority) {
  int tid = mythread_gettid();	
  return t_state[tid].priority;
}


/* Get the current thread id.  */
int mythread_gettid(){
  if (!init){ init_mythreadlib(); init=1;}
  return ejecutando->tid;
}

/* Timer interrupt  */
void timer_interrupt(int sig){
  //Antes de ejecutar el planificador se recorren todos los
  //procesos de baja prioridad para evaluar su atributo hungry
  struct my_struct *ahora =esperando_rr->head;
  while(ahora != NULL){
    TCB* aux = (TCB*)(ahora->data);
    aux->hungry--;
    if (aux->hungry == 0){ //Si hay un proceso que esté sufriendo inanición
      printf("El hilo %d se ha muerto de hambre\n", aux->tid);
      disable_interrupt();
      TCB* hola = queue_find_remove(esperando_rr,aux); //lo eliminamos de la cola de baja prioridad
      if(hola!=NULL){
        printf("Vamos a encolar en la lista de FIFO al proceso %d\n", hola->tid);
      }
      enqueue(esperando_fifo,hola);//Se encola en la de alta prioridad.
      enable_interrupt();
    }
    ahora = ahora->next;  
  }

  if(ejecutando->priority != HIGH_PRIORITY)(ejecutando->ticks)--;
  if(ejecutando->ticks == 0){//el proceso en ejecución se quedó sin rodaja.
    TCB* next = scheduler(); //Vemos si hay un siguiente proceso
    if(next == NULL){//no hay nadie más, hay que seguir ejecutando esto.
      ejecutando->ticks=QUANTUM_TICKS;
      return;
    }//en caso contrario, debemos cambiar de proceso.
    TCB* viejo = ejecutando;//guardamos el proceso viejo.
    ejecutando = next; //actualizamos ejecutando.
    disable_interrupt();
    viejo->hungry = STARVATION;
    enqueue(esperando_rr, viejo); //Encolamos el proceso viejo en esperando_rr.
    enable_interrupt();
    viejo->ticks=QUANTUM_TICKS;
    activator(viejo, next);
  }
      // else{
      //   en principio si no hay término de rodaja, el tick no importa salvo para decrementar la variable.
      // }
} 



/* Scheduler: returns the next thread to be executed */
TCB* scheduler(){
  disable_interrupt();
  TCB* next = dequeue(esperando_fifo);  
  enable_interrupt();
  if(next==NULL){
    disable_interrupt();
    next = dequeue(esperando_rr);
    enable_interrupt();
  }
  return next;
  /*printf("mythread_free: No thread in the system\nExiting...\n");	
  exit(1);*/
}

/* Activator */
void activator(TCB* now, TCB* next){
  if(now ==NULL && next == NULL){
    printf("FINISH\n"); 
    exit(1);
    printf("You should never arrive here\n");
  }
  if(now == NULL){ //cuando el hilo sale.
    setcontext(&(next->run_env));
    printf("mythread_free: After setcontext, should never get here!!...\n");        
    return;
  }
  printf("*** SWAPCONTEXT FROM %d to %d\n", now->tid, next->tid);
  swapcontext(&(now->run_env), &(next->run_env));//el proceso dormido volverá aquí.
}



