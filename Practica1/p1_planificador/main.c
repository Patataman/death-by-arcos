#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <stdlib.h>
#include <ucontext.h>
#include <unistd.h>

#include "mythread.h"


void fun1 (int global_index)
{
  int a=0, b=0;
  for (a=0; a<10; ++a) { 
//    printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
    for (b=0; b<25000000; ++b);
  }

  for (a=0; a<10; ++a) { 
//    printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
    for (b=0; b<25000000; ++b);
  }
  mythread_exit(); 
  return;
}


void fun2 (int global_index)
{
  int a=0, b=0;
  for (a=0; a<10; ++a) {
  //  printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
    for (b=0; b<18000000; ++b);
  }
  for (a=0; a<10; ++a) {
  //  printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
    for (b=0; b<18000000; ++b);
  }
  mythread_exit();
  return;
}

void fun3 (int global_index)
{
  int a=0, b=0;
  for (a=0; a<10; ++a) {
   // printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
    for (b=0; b<40000000; ++b);
  }
  for (a=0; a<10; ++a) {
    //printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
    for (b=0; b<40000000; ++b);
  }
  mythread_exit();
  return;
}
void fun4(){//una función larga de ejecutar para permitir evaluar inanición de un proceso de baja prioridad
  int a=0, b=0;
  for (a=0; a<100; ++a) {
   // printf ("Ejecutando en la fun4() la iteración %d\n", a);
    for (b=0; b<40000000; ++b);
  }
mythread_exit();
}


int main(int argc, char *argv[]){
  int a=0, b=0;

                                ///////////////////////////////PRUEBAS////////////////////////// 
//1.-(aquí se prueban expulsiones y manejo de inanición)
 /* mythread_create(fun4, HIGH_PRIORITY);
  mythread_exit();*/
//2.- Aquí se hace una prueba de RR puro
/*  if(mythread_create(fun3, LOW_PRIORITY) < 0){
    printf("Error creando el hilo\n");
  }
  if(mythread_create(fun3, LOW_PRIORITY) < 0){
    printf("Error creando el hilo\n");
  }
  if(mythread_create(fun5, LOW_PRIORITY) < 0){
    printf("Error creando el hilo\n");
  }
  if(mythread_create(fun3, LOW_PRIORITY) < 0){
    printf("Error creando el hilo\n");
  }
  if(mythread_create(fun3, LOW_PRIORITY) < 0){
    printf("Error creando el hilo\n");
  }
  if(mythread_create(fun3, LOW_PRIORITY) < 0){
    printf("Error creando el hilo\n");
  }
  mythread_exit();*/

//3.- Aquí se prueba FIFO puro se comprueba que el hilo 0 asciende a la cola de alta prioridad pero ejecuta sólo un quantum.
  /*mythread_create(fun3, HIGH_PRIORITY);
  mythread_create(fun3, HIGH_PRIORITY);
  while(a<1000000000)a++;
  mythread_create(fun3, HIGH_PRIORITY);
  mythread_create(fun3, HIGH_PRIORITY);
  mythread_create(fun3, HIGH_PRIORITY);
  mythread_create(fun3, HIGH_PRIORITY);
  mythread_exit();*/
///////////////////////////////////////////////////////////////

 int i,j,k,l,m;

  mythread_setpriority(MEDIUM_PRIORITY);
 
  if((i = mythread_create(fun1,LOW_PRIORITY)) == -1){
    printf("thread failed to initialize\n");
    exit(-1);
  }
  if((j = mythread_create(fun2,LOW_PRIORITY)) == -1){
    printf("thread failed to initialize\n");
    exit(-1);
  }
  if((k = mythread_create(fun3,MEDIUM_PRIORITY)) == -1){
    printf("thread failed to initialize\n");
    exit(-1);
  }  
  if((l = mythread_create(fun1,HIGH_PRIORITY)) == -1){
    printf("thread failed to initialize\n");
    exit(-1);
  }

  if((m = mythread_create(fun2,HIGH_PRIORITY)) == -1){
    printf("thread failed to initialize\n");
    exit(-1);
  }
  
  printf("thread IDs are %d %d %d %d %d\n", i,j,k,l,m);
 
   mythread_create(fun1,LOW_PRIORITY);
    mythread_create(fun1,LOW_PRIORITY);
     mythread_create(fun1,LOW_PRIORITY);
   
      
     
  for (a=0; a<10; ++a) {
  //    printf ("Thread %d with priority %d\t from fun2 a = %d\tb = %d\n", mythread_gettid(), mythread_getpriority(), a, b);
      for (b=0; b<30000000; ++b);
    }	
    mythread_create(fun1,HIGH_PRIORITY);
    
  mythread_exit();	
  printf("This program should never come here\n");
  
  return 0;
} /****** End main() ******/


